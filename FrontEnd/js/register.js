
const username = document.getElementById("username");
const password = document.getElementById("password");
const region = document.getElementById("region");
const city = document.getElementById("city");
const email = document.getElementById("email");
const mobile_no = document.getElementById("mobile_no");
const submit = document.getElementById("register");

region.addEventListener("click", enablecity);


function onlyNumberKey(evt) { //only number in mobile no textbox
        
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}

function enablecity(){ //change of disabled of dropdown-city
    if(region.value){
        document.getElementById("city").disabled = false;
    }else{
        document.getElementById("city").disabled = true;
    }
}